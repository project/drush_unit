<?php

require_once(realpath(dirname(__FILE__) . '/../configure.php'));
require_once(realpath(dirname(__FILE__) . '/../simpletest/autorun.php'));

class AllTests extends TestSuite {
  function __construct() {
    $this->TestSuite('All tests');
  }
}
