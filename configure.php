<?php

if (!defined('DRUSH_ROOT')) {
  $drush_root_env = getenv('DRUSH_ROOT');
  if ($drush_root_env) {
    // If the user has set a Drush root environment variable,
    // use that.
    define('DRUSH_ROOT', $drush_root_env);
  } else {
    // Assume that Drush Unit is installed in $drush_root/unit,
    // and therefore the path to drush is above this file.
    define('DRUSH_ROOT', realpath(dirname(__FILE__) . '/../drush'));
  }
}
